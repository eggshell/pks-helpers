#!/bin/bash

kubectl apply -f k8s/vsphere-storage-class.yaml

./scripts/install-helm.sh
./scripts/deploy-ingress-controller.sh
./scripts/deploy-gitlab.sh
