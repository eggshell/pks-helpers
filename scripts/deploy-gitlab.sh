#!/bin/bash

# internal ip of lab environment load balancer provisioned by ingress controller
export LB_IP_ADDR="24.24.24.17"
export EMAIL="ctaylor@gitlab.com"
export DOMAIN="pks.tanuki.host"

echo "load balancer ip: $LB_IP_ADDR"
echo "email: $EMAIL"
echo "domain: $DOMAIN"

helm upgrade --install gitlab gitlab/gitlab                      \
             --timeout 600                                       \
             --set global.hosts.externalIP=${LB_IP_ADDR}         \
             --set global.hosts.domain=${DOMAIN}                 \
             --set gitlab-runner.install=false                   \
             --set certmanager.install=false                     \
             --set global.ingress.configureCertmanager=false     \
             --set nginx-ingress.enabled=false                   \
             --set global.ingress.class="nginx"                  \
             --set tcp.22="default/gitlab-gitlab-shell:22"       \
             --set global.persistence.storageClass="sc-standard"
