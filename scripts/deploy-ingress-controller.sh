#!/bin/bash

helm repo update
helm install stable/nginx-ingress   \
             --name nginx           \
             --set rbac.create=true \
             --set controller.config.proxy-buffer-size=16k
