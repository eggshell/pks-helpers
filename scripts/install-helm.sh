#!/bin/bash

curl https://raw.githubusercontent.com/kubernetes/helm/master/scripts/get | bash
kubectl -n kube-system create sa tiller
kubectl create clusterrolebinding tiller \
        --clusterrole cluster-admin      \
        --serviceaccount=kube-syystem:tiller

helm init --skrip-refresh --upgrade --service-account tiller
